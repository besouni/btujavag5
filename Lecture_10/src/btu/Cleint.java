package btu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Cleint extends Thread{
    Socket socket;
    String message;
    ObjectOutputStream out;
    ObjectInputStream in;

    @Override
    public void run() {
        try {
            while (true) {
                socket = new Socket(InetAddress.getByName("localhost"), 8080);
                Scanner scanner = new Scanner(System.in);
                message = scanner.nextLine();
                out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(message);
                in = new ObjectInputStream(socket.getInputStream());
                message = (String) in.readObject();
                System.out.println("Server - >" + message);
                if(message.equals("bye")){
                    System.exit(0);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
