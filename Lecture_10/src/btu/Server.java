package btu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server implements Runnable {
    Socket socket;
    ObjectInputStream in;
    String messageServer;
    ObjectOutputStream out;

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(8080);
            while (true) {
                socket = serverSocket.accept();
                in = new ObjectInputStream(socket.getInputStream());
                messageServer = (String) in.readObject();
                System.out.println("Client -> " + messageServer);
                Scanner scanner = new Scanner(System.in);
                messageServer = scanner.nextLine();
                out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(messageServer);
                if(messageServer.equals("bye")){
                    System.exit(0);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
