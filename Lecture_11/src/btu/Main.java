package btu;


interface Lambda{
  public void method(int x, int y);
}

public class Main {

    public static void main(String[] args) {
	// write your code here

        Lambda l1 = (x, y) -> {
            System.out.println(x+y);
        };

        Lambda l2 = (x, y) -> {
            System.out.println(x*y);
        };

        l1.method(3, 4);
        l2.method(3, 4);

//        SubBase subBase = new SubBase();
//        subBase.method1();
//        int x = 9;
//        Integer y = x;
//        int z = y;



//        Categoria cat1 = Categoria.XILI;
//        cat1.method1();
//        switch (cat1){
//            case XORCEULI -> {
//                System.out.println("Chatvirtva Xorceuli");
//            }
//            case XILI -> {
//                System.out.println("Xili");
//            }
//            default -> {
//                System.out.println("Default");
//            }
//        }

//        switch (cat1){
//            case XILI:
//                System.out.println("Xili");
//                break;
//            case XORCEULI:
//                System.out.println("Xorceuli");
//                break;
//            default:
//                System.out.println("Default");
//        }


//        Test t = new Test();
//        t.method1(4);
//        t.method1(4.5);
//        t.method3(4);
//        t.method3(4.5);

    }
}
