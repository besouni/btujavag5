package btu;

public class SubBase extends Base{
    @Override
    public void method(){
        System.out.println("Sub Base");
    }

    @Deprecated
    public void method1(){
        System.out.println("Method 1");
    }

    public void method2(){
        System.out.println("Method 2");
    }
}
