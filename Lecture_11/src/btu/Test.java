package btu;

public class Test <TIME, GPA> {
    TIME time;
    GPA gpa;
    public void method1(Integer time){
        System.out.println(time.getClass().getTypeName());
    }

    public void method2(Object time){
        System.out.println(time.getClass().getTypeName());
    }

    public <TIME> void method3(TIME t){
        System.out.println(t.getClass().getTypeName());
    }
}
