package btu;

import java.sql.*;

public class DB {
    Connection connection;
    String mysqlConnection = "jdbc:mysql://localhost:3306/javabtu5";
    public DB(){
        try {
            connection = DriverManager.getConnection(mysqlConnection, "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id){
        String query = "DELETE FROM users WHERE id=?";
        try {
            PreparedStatement pr = connection.prepareStatement(query);
            pr.setInt(1, id);
            pr.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void insert(String username, String password, String email){
        String query = "INSERT INTO users(username, password, email) VALUES (?, ?, ?)";
        try {
            PreparedStatement pr =  connection.prepareStatement(query);
            pr.setString(1, username);
            pr.setString(3, email);
            pr.setString(2, password);
            pr.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void select(){
//        int id = 8;
        String query = "SELECT * FROM users";
        try {
            Statement stm = connection.createStatement();
            ResultSet  result = stm.executeQuery(query);
            while(result.next()) {
                System.out.print(result.getString("username") + " - ");
                System.out.println(result.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
