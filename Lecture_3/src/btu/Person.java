package btu;

public class Person {
    String name = "Anna";
    String lastname;
    int age;
    int m[];

    public void setName(String name) {
        this.name = name;
    }

    public Person() {
        System.out.println("Constructor is running!!!");
        m = new int []{4, 5, 3 };
    }

//
    public Person(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                '}';
    }
}
