package btu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Before Error");
//        Error error = new Error();
//        error.Test();
//        CompileException compileException = new CompileException();
//        compileException.Test();
        RuntimeException runtimeException = new RuntimeException();
        runtimeException.Test();
        System.out.println("After Error");
    }
}
