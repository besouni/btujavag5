package btu;

public class RuntimeException {
    public RuntimeException(){
        System.out.println("Constructor is running!!!");
    }

    public void Test(){
        int x = 9, y = 0;
        int m[] = {3, 4, 5};
        String s = "Hello";
        try{
            System.out.println(s.charAt(10));
            System.out.println(m[0]);
            System.out.println(x/y);
        }catch(Exception e){
            System.out.println("Exception - > ");
        }

//        try{
////            System.out.println(s.charAt(10));
//            System.out.println(m[0]);
//            System.out.println(x/y);
//        }catch(ArithmeticException e){
//            System.out.println("Arithmetic - > ");
//        }catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("Array - > ");
//        }finally {
//            System.out.println("Finally");
//        }

        System.out.println("After Exception!!!");
    }
}
