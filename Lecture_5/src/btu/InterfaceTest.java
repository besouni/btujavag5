package btu;

public class InterfaceTest implements Interface1 {
    @Override
    public void method1() {
        System.out.println("Hello Java");
    }

    @Override
    public int method2() {
        return 0;
    }

    @Override
    public int method3(int x, float y, String s) {
        return 0;
    }
}
