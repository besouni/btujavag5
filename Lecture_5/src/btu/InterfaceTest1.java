package btu;

public class InterfaceTest1 implements Interface1, Interface2{
    @Override
    public void method1() {
        System.out.println("Hello HTML");
    }

    @Override
    public int method2() {
        return 0;
    }

    @Override
    public int method3(int x, float y, String s) {
        return 0;
    }

    @Override
    public void method2_9() {
       this.method2_1();
    }
}
