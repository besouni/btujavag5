package filesandfolders;

import javax.swing.*;
import java.io.*;

public class TestFilesAndFolders {

    public void FileReaderMethod(){
        String url2 = "D:\\file1.txt";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(url2));
//            System.out.println(bufferedReader.readLine());
//            System.out.println(bufferedReader.readLine());
//            System.out.println(bufferedReader.readLine());
            String s;
            while ((s=bufferedReader.readLine())!=null){
                System.out.println(s);
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("==============================");
        String url1 = "D:\\file2.txt";
        try {
            FileReader fileReader = new FileReader(url1);
//            System.out.println(fileReader.read());
//            System.out.println(fileReader.read());
//            System.out.println(fileReader.read());
//            System.out.println(fileReader.read());
//            System.out.println(fileReader.read());
            int s;
            while ((s=fileReader.read())!=-1){
                System.out.println((char) s);
            }

            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void FileMethod1(){
        String url = "D:\\file.txt";
        File file = new File(url);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void FileWriterMethod(){
        String url1 = "D:\\file1.txt";
        try {
            FileWriter fileWriter = new FileWriter(url1, true);
            fileWriter.write("\nJava");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        String url2 = "D:\\file2.txt";
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(url2));
            bufferedWriter.write("Java");
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
