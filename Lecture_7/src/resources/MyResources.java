package resources;

public class MyResources {
    int N = 1000;
    public synchronized void changeN(int m, int c, String  name){
        for(int i=0; i<c; i++){
            System.out.println(name+" - "+N);
            if(N>0) {
                N -= m;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
