package resources;

public class TestThread extends Thread{
    MyResources myResources;

    public TestThread(MyResources myResources){
        this.myResources = myResources;
    }
    @Override
    public void run() {
        myResources.changeN(200, 5, "TestThread");
    }
}
