package btu;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Object [] m1 = new Object[5];
        m1[0] = 4;
        m1[1] = 9.9;
        m1[2] = "JavaFx";
        System.out.println(Arrays.toString(m1));
        int [] m2 = {3, 4, 5, 5};
        ArrayList a1 = new ArrayList();
        ArrayList <Object> a3 = new ArrayList<Object>();
        a1.add(3);
        a1.add(5.6);
        a1.add("Java");
        a1.add(true);
        ArrayList <Integer> a2 = new ArrayList<Integer>();
        a2.add(5);
        System.out.println(a1);
        a1.add(2, 98);
        System.out.println(a1);
        Object [] m4 = {3, 4, 5};
        int [] m5 = {2, 3, 4};
        a1.addAll(3, Arrays.asList(m4));
//        a1.addAll(2, Collections.singleton(m5));
        System.out.println(a1);

        List l = new ArrayList();
        List l1 = new LinkedList();
        List l2 = new Vector();

        Set s = new HashSet();
        s = new TreeSet();
        s.add(87);
        s.add(-5);
        s.add(3);
        s.add(3);
        s.add(2);
        s.add(5);
        s.add(2);
        s.add(-9);
        System.out.println(s);

        System.out.println(a1.size());

        HashMap h1 = new HashMap();
        Hashtable h2 = new Hashtable();
//        HashMap <Integer, Object> h2 = new HashMap<>();
        h1.put("lang", "Java");
        h1.put("tech", "JVM");
        h1.put(1, "Hello");
        System.out.println(h1);
        System.out.println(h1.get("lang"));
//        System.out.println(h1["lang"]);
    }
}
