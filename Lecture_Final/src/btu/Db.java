package btu;

import java.sql.*;

public class Db {
    Connection connection;
    public Db(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javabtu3", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet select(){
        ResultSet result =  null;
        try{
            String sql = "SELECT * FROM users";
            Statement statement = connection.createStatement();
            result = statement.executeQuery(sql);
        }catch (Exception e){

        }
        return result;
    }
}
