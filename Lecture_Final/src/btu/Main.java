package btu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
        მონაცემთა ბაზის ერთ ცხრილიდან მონაცემების:
            გამოტანა;
            ჩაწერა;
            რედაქტირება;
            წაშლა.
        შესაბამისი JavaFx-ის გარემოს აგება.
*/
public class Main extends Application {

    public static void main(String[] args) {
	// write your code here
        launch();
    }



    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Home");
        stage.show();
    }
}
